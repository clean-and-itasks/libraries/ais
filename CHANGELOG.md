# Changelog

#### 1.0.1

- Chore: accept `base` `3.0`.

## 1.0.0

- Initial version.
